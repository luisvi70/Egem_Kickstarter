/******************************************************************************/
//
//  EGEM Crowdfunding - test/CampaignLottery.test.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
const assert  = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory  = require('../Egem/build/CampaignFactory.json');
const compiledCampaign = require('../Egem/build/Campaign.json');

let accounts;
let factory;
let campaignAddress;
let campaign;
let managerAddress;
let managerNickname = 'Manager';
let minimumContribution = '100';
let lotteryPercentage = '10';

describe('-------------------------- Test Lottery Distribution ---------------------------------------', () => {

  let winner = [0, 0, 0, 0, 0, 0, 0, 0, 0];
  let numTests = 10;

  beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    managerAddress = accounts[0];

    // managerAddress is the Factory owner
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
      .deploy({ data: compiledFactory.bytecode })
      .send({ from: managerAddress, gas: '6000000' });

    // managerAddress (Manager) creates a new campaign
    await factory.methods.createCampaign(minimumContribution,'Summary',managerNickname,'URL','10').send ({
      from: managerAddress,
      gas: '2000000'
    });

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
  });

  // This test is setup to verify the Lottery winnigs distribution over a large
  // number of events, defined by numTests
  for (var numRequest = 1; numRequest <= numTests; numRequest++) ( (_numTests, _numRequest) => {
    it('Manager approves a withdrawal request', async () => {
      // Lets begin by making 9 contributions to the Campaign
      const contribute = web3.utils.toWei('0.01', 'ether');
      for (var i = 1; i < 10; i++) {
        await campaign.methods.contribute().send({
          value: contribute,
          from: accounts[i],
          gas: '2000000'
        });
      }

      // Lets keep track of Manager account balance at the beginning
      var prevBalance = await web3.eth.getBalance(managerAddress);
      balance = web3.utils.fromWei(prevBalance, 'ether');
      prevBalance = parseFloat(balance);

      // Campaign owner creates a withdrawal request
      const amount = web3.utils.toWei('0.09', 'ether');
      await campaign.methods
        .createRequest('A', amount, managerAddress)
        .send({
          from: managerAddress,
          gas: '2000000'
        });

      // The Manager approves the withdrawal request
      await campaign.methods.approveRequest(0).send({
        from: managerAddress,
        gas: '2000000'
      });

      // The Manager finilizes the withdrawal request.
      await campaign.methods.finalizeRequest(0).send({
        from: managerAddress,
        gas: '2000000'
      });

      // Lets check request data
      request = await campaign.methods.requests(0).call();
      assert.equal('A', request.description);
      assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
      assert.equal(managerAddress, request.recipient);
      assert(request.complete);
      assert.equal(request.approvalCount, '1');
      // Lets record Lottery winner info for statistical analysis.
      for (var i = 0; i < 9; i++) {
        // accounts[0] is not a contributor
        // accounts[1] to accounts[9] contributed to the Campaign, lets keep track
        // who was the Lottery winner for this withdrawal request cycle
        if (request.lotteryWinner == accounts[i+1]) winner[i]++;
      }
      sumArray = winner.reduce((a,b) => a + b);
      assert.equal(sumArray,_numRequest);
      console.log(`Winners Array for cycle (${_numRequest}/${_numTests}): `, winner);

      var newBalance = await web3.eth.getBalance(managerAddress);
      balance = web3.utils.fromWei(newBalance, 'ether');
      newBalance = parseFloat(balance);
      assert(newBalance > prevBalance);
    })
  })(numTests, numRequest)
});
