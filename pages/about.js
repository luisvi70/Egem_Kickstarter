/******************************************************************************/
//
//  EGEM Crowdfunding - about.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Container, Header } from 'semantic-ui-react';
import Layout from '../components/Layout';

class About extends Component {

  render() {
    return (
      <Container style={{marginBottom: '40px'}}>
      <Layout>
        <Header
          as='h2'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='center'>
          About This Smart Contract
        </Header>

        <Header
          as='h2'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='left'>
          How does it work?
        </Header>

        <p>
        This smart contract has been deployed to the EtherGem blockchain network. It is composed of two primary contracts:
        (a) a factory contract and (b) a campaign contract. To intercat with the contract, you will need to make use of the
        Metamask extension in your browser. For help on how to properly setup Metamask for Egem support, please consult this link:
        <ul>
          <li>
            <a href="https://wiki.egem.io/en/egemwallet" target="_blank">
            Setup Metamask for EGEM.
            </a>
          </li>
        </ul>
        </p>

        <p>
        The factory contract takes care of related actions when an EtherGem developer creates a new crowdfunding project.
        When a new project (or campaign) is created, the project (or campaign) creates an instance of a campaign smart
        contract which is deployed to the EtherGem network.
        </p>

        <p>
        The campaign contract takes control of all the actions associated to a project (or campaign). Such actions, include:
        <ul>
          <li>allocate contribution funds into the contract.</li>
          <li>allows the developer to create requests to withdraw funds.</li>
          <li>allows EtherGem core developers to approve any withdrawal requests from developers.</li>
          <li>allows the developer to finalize a project (or campaign).</li>
          <li>manage the Lottery logic to send Lottery funds to one lucky project contributor each time a funds withdrawal request is finilized.</li>
        </ul>
        </p>

        <Header
          as='h2'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='left'>
          Benefits for EtherGem developers
        </Header>
        <p>
        It provides a mechanism to help community developers to get a well deserved retribution for enriching the
        EtherGem platform.
        </p>

        <Header
          as='h2'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='left'>
          Benefits for EtherGem community
        </Header>
        <p>
        Community members that contribute to support projects are entered into a lottery. When funds are approved
        and withdrawn from the project, a percentage of the funds is locked and distributed to one lucky contributor
        of the project.
        </p>

      </Layout>
      </Container>
    );
  }
}

export default About;
