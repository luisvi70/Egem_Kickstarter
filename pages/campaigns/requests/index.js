/******************************************************************************/
//
//  EGEM Crowdfunding - requests/index.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Container, Label, Table } from 'semantic-ui-react';
import { Link } from '../../../routes';
import Layout from '../../../components/Layout';
import RequestRow from '../../../components/RequestRow';
import Campaign from '../../../Egem/campaign';
import web3 from '../../../Egem/web3';
import factory from '../../../Egem/factory';

class RequestIndex extends Component {

  state = {
    loadingAddRequest: false
  };

  static async getInitialProps(props) {
    const accounts = await web3.eth.getAccounts();
    const { address } = props.query;
    const campaign = Campaign(address);
    // Get Campaign contract data
    const summary = await campaign.methods.getSummary().call();
    const requestCount = await campaign.methods.getRequestsCount().call();
    const approversCount = await campaign.methods.approversCount().call();
    const campaignOwner = await campaign.methods.manager().call();
    const isApprover = await campaign.methods.approvers(accounts[0]).call();
    // Get CampaignFactory contract data
    const devNickname = await factory.methods.devNickname(summary[4]).call();
    const isCoreDev = await factory.methods.isCoreDev(accounts[0]).call();
    const campaignFinalized = await factory.methods.campaignFinalized(props.query.address).call();
    var isCampaignOwner = false;

    if (accounts[0] == campaignOwner) isCampaignOwner=true;

    const requests = await Promise.all(
      Array(parseInt(requestCount)).fill().map((element, index) => {
        return campaign.methods.requests(index).call();
      })
    );

    const hasApproved = await Promise.all(
      Array(parseInt(requestCount)).fill().map((element, index) => {
        return campaign.methods.hasApprovedRequest(index,accounts[0]).call();
      })
    );

    return {
      address:        address,
      balance:        web3.utils.fromWei(summary[1],'ether'),
      campaignOwner:  campaignOwner,
      requests:       requests,
      requestCount:   requestCount,
      approversCount: approversCount,
      isApprover:     isApprover,
      isCoreDev:      isCoreDev,
      isCampaignOwner:isCampaignOwner,
      hasApproved:    hasApproved,
      campaignFinalized: campaignFinalized,
      devNickname:    devNickname
     };
  }

  renderRows() {
    return this.props.requests.map((request, index) => {
      return <RequestRow
        key={index}
        id={index}
        request={request}
        address={this.props.address}
        approversCount={this.props.approversCount}
        isApprover={this.props.isApprover}
        isCoreDev={this.props.isCoreDev}
        isCampaignOwner={this.props.isCampaignOwner}
        hasApproved={this.props.hasApproved[index]}
      />;
    });
  }

  render() {
    const { Header, Row, HeaderCell, Body } = Table;

    return(
      <Layout>
        <Header
          as='h2'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
          Pending Withdrawal Requests
        </Header>

        <Header
          as='h3'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
          Campaign owner: {this.props.devNickname}
        </Header>

        <Header
          as='h3'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
          Withdrawal Address: {this.props.campaignOwner}
        </Header>

        {this.props.isCampaignOwner ?
          <div>
          <Label>You are the Campaign Owner</Label>
          <Label>Available Balance: {this.props.balance} EGEM</Label>
          <Link route={`/campaigns/${this.props.address}/requests/new`}>
            <a>
              <Button
                primary
                floated="right"
                disabled={this.props.campaignFinalized}
                loading={this.state.loadingAddRequest}
                onClick={event => this.setState({ loadingAddRequest: true })}
                style={{ marginBottom: 10 }}>
                Add Request
              </Button>
            </a>
          </Link>
          </div>
        : null }

        <Table>
          <Header>
            <Row>
              <HeaderCell>ID</HeaderCell>
              <HeaderCell>Description</HeaderCell>
              <HeaderCell>Amount (EGEM)</HeaderCell>
              <HeaderCell>Lottery Winner</HeaderCell>
              <HeaderCell>Approval Count</HeaderCell>
              <HeaderCell>Approve</HeaderCell>
              <HeaderCell>Finalize</HeaderCell>
            </Row>
          </Header>
          <Body>
            {this.renderRows()}
          </Body>
        </Table>
        <div>Found {this.props.requestCount} requests.</div>
      </Layout>
    );
  }
}

export default RequestIndex;
