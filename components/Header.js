/******************************************************************************/
//
//  EGEM Crowdfunding - components/Header.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Container, Grid, Icon, Label, Message, Segment } from 'semantic-ui-react';
import { Link } from '../routes';
import factory from '../Egem/factory';
import web3 from '../Egem/web3';

class Header extends Component {

  state = {
    loadingHome: false,
    loadingAbout: false,
    loadingEgemio: false,
    activeMetamask: '',
    isManager: false,
    isCoreDev: false,
    isCommDev: false
  };

  constructor(props) {
    super(props);
    this.checkAccount();
  }

  async checkAccount() {
    const accounts = await web3.eth.getAccounts();

    if (accounts.length != 0) {
      this.setState({ activeMetamask: accounts[0] });
      // Verify if account is contract Manager.
      const managerAddress = await factory.methods.manager().call();
      if (accounts[0] == managerAddress) {
          this.setState({ isManager: true });
      }
      const devData = await factory.methods.getDevData(accounts[0]).call();
      const isCoreDev = devData[0];
      const isCommDev = devData[1];
      this.setState({ isCoreDev: isCoreDev });
      this.setState({ isCommDev: isCommDev });
    }
  }

  render() {
    return (
    <Container>
    <Grid columns={2} style={{ marginTop: '40px', color: 'white' }}>
      <Grid.Row>
        <Grid.Column floated='left'>
          <Link route="https://egem.io/">
            <a><img src="/static/EGEM-logo.png" alt="EtherGem Logo" width="200" /></a>
          </Link>
        </Grid.Column>

        <Grid.Column floated='right'>
          <Link route="/">
          <Button
              disabled={this.state.loadingHome}
              onClick={event => {this.setState({loadingHome: true})}}
              size='mini'>
              <Icon name='home' />
              <a>
              Home
              </a>
          </Button>
          </Link>

          <Link route="/about">
          <Button
              disabled={this.state.loadingAbout}
              onClick={event => {this.setState({loadingAbout: true})}}
              size='mini'>
              <Icon name='book' />
              <a>
              About
              </a>
          </Button>
          </Link>

          <Button
              loading={this.state.loadingEgemio}
              onClick={event => {this.setState({loadingEgemio: true})}}
              size='mini'>
              <Icon name='chain' />
            <a href="https://egem.io/">
              egem.io
            </a>
          </Button>

        </Grid.Column>
      </Grid.Row>

      <Grid.Row>
        <Grid.Column floated='right' width={8}>
        <Segment raised style={{ color: 'black' }}>
          <Label
            color={this.state.activeMetamask ? 'green':'red'}
            ribbon>
              Metamask Account
          </Label>
          {this.state.activeMetamask ?
             <span>{this.state.activeMetamask}</span>:
             <span>Metamask is Not Connected</span>}
        </Segment>
        { this.state.isManager ?
          <Label
            color={this.state.isManager ? 'green':'red'}>
              Manager
          </Label>
           : null
        }
        <Label
          color={this.state.isCoreDev ? 'green':'red'}>
            Core Dev
        </Label>
        <Label
          color={this.state.isCommDev ? 'green':'red'}>
            Comm. Dev
        </Label>
        </Grid.Column>
      </Grid.Row>
    </Grid>
    </Container>
  );
  }
}

export default Header;
