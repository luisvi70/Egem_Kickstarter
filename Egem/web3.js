/******************************************************************************/
//
//  EGEM Crowdfunding - Egem/web3.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import Web3 from 'web3';

let web3;

if (typeof window !== 'undefined' && typeof window.ethereum !== 'undefined') {
  console.log("(web3.js) Provider -- window");
  // We are in the browser and Metamask is running
  // We tell our instance of Web3 to make use of the Metamask provider
  console.log("(web3.js) Provider -- window.ethereum");
  web3 = new Web3(window.ethereum);
  try {
      window.ethereum.enable().then(function() {
          // User has allowed account access to DApp...
      });
   } catch(e) {
      // User has denied account access to DApp...
   }
} else {
  console.log("(web3.js) Egem Network Provider");
  const rpcURL = "https://lb.rpc.egem.io";
  const provider = new Web3.providers.HttpProvider(rpcURL);
  web3 = new Web3(provider);
}

export default web3;
